void main() {
  // Set<dataType> variableName = {values};

  // A set in Dart is an unordered collection of unique items
  Set<String> subContractors = {'Sonderhoff', 'Stahlsmidt'};

  subContractors.add('Schweisstechnik');
  subContractors.add('Kreatel');
  subContractors.add('Kunstoffe');
  subContractors.remove('Sonderhoff');
  print(subContractors);
}
