void main() {
  Map<String, String> address = {
    'specifics': '221B Baker Street',
    'district': 'Marylebone',
    'city': 'London',
    'country': 'United Kingdom',
  };

  address['region'] = 'Europe';

  print(address);
}
