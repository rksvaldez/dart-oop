void main() {
  Map<String, dynamic> person = {
    'firstName': 'John',
    'lastName': 'Smith',
    'age': '34',
  };

  List<String> country = [
    'Thailand',
    'Philippines',
    'Canada',
    'Singapore',
    'United Kingdom',
    'Japan',
  ];

  List<String> pcPart = [
    'Ryzen 5 3600',
    'Ryzen 3 3200G',
    'Ryzen 7 5800X',
    'Ryzen 9 5950X',
  ];

  print(pcPart.length);
  print(country.length);
  print(person.length);
}
