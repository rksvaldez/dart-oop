import './freezed_models/user.dart';

void main() {
    User userA = User(id: 1, email: 'john@gmail.com');
    User userB = User(id: 1, email: 'john@gmail.com');

    print(userA.hashCode);
    print(userB.hashCode);
    print(userA == userB);

    // Demonstration of object immutbility below.
    // Immutbility means changes are not allowed.
    // It ensures that an object will not be changed accidentally.

    // Instead of directly changing the object's property,
    // the object itself will be changed.
    // To achieve this, we use the object.copyWith() method.

    // print(userA.email);
    // userA = userA.copyWith(email: 'john@hotmail.com');
    // print(userA.email);

    var response = {'id':3, 'email': 'doe@gmail.com'};

    // User userC = User(
    //    id: response['id'] as int, 
    //    email: response['email'] as String
    // );

    User userC = User.fromJson(response);
    print(userC);
    print(userC.toJson());
}