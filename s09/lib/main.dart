    import 'package:flutter/material.dart';
    import './body_questions.dart';
    import './body_answers.dart';

    void main(){
    runApp(App());
    }

    class App extends StatefulWidget{
    @override
    AppState createState() => AppState();
    }

    class AppState extends State<App> {
    int questionIdx = 0;
    bool showAnswers = false;

    final questions = [
    {
        'question': 'What is the nature of your business needs?',
        'options': ['Time Tracking', 'Asset Management', 'Issue Tracking'],
    },

    {
        'question': 'What is the expected size of the user base?',
        'options': ['Less than 1,000', 'Less than 10,000', 'More than 10,000'],
    },

    {
        'question': 'In which region would the majority of the user base be?',
        'options': ['Asia', 'Europe', 'Americas', 'Africa', 'Middle East'],
    },

    {
        'question': 'What is the expected project duration?',
        'options': ['Less than 3 months', '3-6 months', '6-9 months', '9-12 months', 'More than 12 months']
    }
];

    var answers=[];

    void nextQuestion(String? answer){
        answers.add({
            'question':questions[questionIdx]['question'],
            'answer': (answer==null)?'':answer
        });

        if (questionIdx < questions.length - 1){
            setState(() => questionIdx++);
        } else {
            setState(() => showAnswers = true);
        }
    }

    @override
    Widget build(BuildContext context) {
        var bodyQuestions = BodyQuestions(questions: questions, questionIdx :questionIdx, nextQuestion: nextQuestion);
        var bodyAnswers = BodyAnswers(answers:answers);

        Scaffold homepage = Scaffold(
            appBar: AppBar(title: Text('Homepage')),
            body: (showAnswers) ? bodyAnswers : bodyQuestions
        );

        return MaterialApp(
            home: homepage
        );
    }
}