//The keyword 'void' means the function will not return anything.
//The syntax of a function is: return-type function-name(parameters){}
//To run click Run or F5
//Ctr + ` >> to hide the debug console
//CTRL shift y >> labas yung debug console
void main() {
  //How do we create variables in Dart?
  var name = 'John';
  // What if you change the value of name? Here is where Dart, kapag nassignan na, di na mapapalitan ng value
  name = 1.toString();
  var number = 1;
  name = number.toString();
  //If sure ka sa data type:
  String firstName = 'Jane';
  String lastName = 'Smith';
  int age = 31; //Integers: numbers without decimal points
  double height = 172.95; // Double - numbers with decimal points
  num weight = 64.92; //allows numbers with or without decimal points
  bool isRegistered = false; //Boolea: TRUE or FALSE
  String? middleName = null; // '?' this allows a variable to accept null value.
  //List = Array:
  List<num> grades = [98.2, 89, 87.88, 91.2];
  //Map = Object, one methid is to create new Map
  //Map person = new Map();
  //OR:
  // Map <String, dynamic> person = {
  //   'name':'Brandon',
  //   'batch': 213
  // };
  Map<String, dynamic> personA = {'name': 'Brandon', 'batch': 213};
  //Alternative syntax for declaring object.
  Map<String, dynamic> personB = new Map();
  personB['name'] = 'Juan';
  personB['batch'] = 89;
  //when we create a map, we can exlpicitly decalre the data types
  //use datatype : dynamic, kung gusto mong iba-iba yung data type.
  //Note, you can ommit the data types when creating Maps and Lists. But with Lists, it is best practice to have related data, so mas maganda kung i declare yung data type when creating a list.
  //Declaring constant variable:
  //final - pwede wag muna lagyan ng value. Pero after kapag nilagyan mo na ng value, final na yun
  final DateTime now = DateTime.now();
  //const - di na pwedeng palitan, so kapag nag declare ka ng const, dapat may value na agad. an identifier must have a corresponding declaration of value.
  const String companyAcronym = 'FFUF';
  //companyAcronym = 'FFUF';
  print('Full Name: $firstName $lastName');
  print('Age: $age');
  print('Age ' + age.toString());
  print('weight: ' + weight.toString());
  print('Grades ' + grades.toString());
  print(isRegistered);
  print(height);
  print(weight);
  print(personA);
  print(personB);
  print('Current Datetime: ' + now.toString());
  //JS: console.log
  //Java System.out.println
  print('Hello ' + name); // The 'Hello' is concatenated with the name variable
  //Kung maraming error? Check the problems tab in the console
  //JS is loosely typed language, solution, they created TypeScipt.. But binabalik pa rin nya into JS
  ////Dart ----> TS -> JS
  ///
  //Acces elements in a map:
  print(personB['name']);
  //Access value in a List
  print(grades[0]);
}
