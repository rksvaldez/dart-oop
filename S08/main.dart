void main(){
// Create objects for each of the vehicle type with the following information.
// - Bulldozer: Caterpillar D10, U blade
// - Tower Crane: 370 EC-B 12 Fibre, 78m hook radius, 12,000kg max capacity
// - Loader: Volvo L60H, wheel loader, 16530lbs tipping load
  Bulldozer bulldozer = new Bulldozer(
    vehicleName: 'Caterpillar D10', 
    bladeType: 'U blade'
    );

  TowerCrane towerCrane = new TowerCrane(
    vehicleName: '370 EC-B 12 Fibre', 
    radius: 78, 
    maxCapacity: 12000
    );

  Loader loader = new Loader(
    vehicleName: 'Volvo L60H',
    equipmentType: 'wheel loader',
    tippingLoad: 16530
    );

  // Inside the main() method, create a List for the
  // equipment (using the Equipment type) and give it an initial value of [].
  List <Equipment> equipmentType = [];

  // Add the created objects to the list created earlier.
  equipmentType.add(bulldozer);
  equipmentType.add(towerCrane);
  equipmentType.add(loader);

  // Loop through each vehicle in the list and execute the describe() method.
  equipmentType.forEach((element){
    print(element.describe());
  });
}

// Create an abstract class named Equipment and an
// abstract method named describe inside the abstract class.
abstract class Equipment{
  String describe();
}

// Create the classes Bulldozer, TowerCrane, and Loader
// that will all implement the abstract class Equipment.
class Bulldozer implements Equipment{
  String vehicleName;
  String bladeType;

  Bulldozer({
    required this.vehicleName, 
    required this.bladeType}
    );

  String describe() {
    return 'The bulldozer ${this.vehicleName} has a ${this.bladeType}';
  }
}

class TowerCrane implements Equipment{
  String vehicleName;
  int radius;
  int maxCapacity;

  TowerCrane({
    required this.vehicleName,
    required this.radius,
    required this.maxCapacity
    });

  String describe(){
    return 'The tower crane ${this.vehicleName} has a radius of ${this.radius} and a max capacity of ${this.maxCapacity}';
  }
}

class Loader implements Equipment{
  String vehicleName;
  String equipmentType;
  int tippingLoad;

  Loader({
    required this.vehicleName,
    required this.equipmentType,
    required this.tippingLoad
    });

  String describe(){
    return 'The loader ${this.vehicleName} is a ${this.equipmentType} and has a tipping load of ${this.tippingLoad} lbs';
  }
}
