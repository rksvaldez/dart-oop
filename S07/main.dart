import './worker.dart';

void main() {
  Doctor doctor = new Doctor(firstName: 'John', lastName: 'Smith');
  Carpenter carpenter = new Carpenter();

  print(carpenter.getType());
}

abstract class Person {
  String getFullName();
}

class Doctor implements Person {
  String firstName;
  String lastName;

  Doctor({required this.firstName, required this.lastName});

  String getFullName() {
    return 'Dr. ${this.firstName} ${this.lastName}';
  }
}
