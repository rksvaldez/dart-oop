void main() {
  Function discountBy20 = getDiscount(20);
  Function discountBy50 = getDiscount(50);

  //The discountBy25 is then considered as a closure.
  //The closure has access to variables in its lexical scope

  print(discountBy20(288.3));
  print(discountBy50(1400));

  print(getDiscount(20)(1400));
  print(getDiscount(50)(1400));
}

Function getDiscount(num percentage) {
  //When the getDiscount is used and the function below is return
  // the value of 'percentage' parameter is retained/
  //That is called lexical scope
  return (num amount) {
    return (amount * percentage / 100);
  };
}
