void main() {
  print(getCompanyName());
  print(getYearEstablishmed());
  print(hasOnlineClasses());
  print(getCoordinates());
  print(combineAddress(
      '134 Timog Ave', 'Brgy, Sacred Heart', 'Quezon City', 'Metro Manila'));
  print(combineName('John', 'Smith'));
  print(combineName('John', 'Smith', isLastNameFirstName: true));
  print(combineName('John', 'Smith', isLastNameFirstName: false));

  List<String> persons = ['John Doe', 'Jane Doe'];
  List<String> students = ['Nicholas Rush', 'James Holden'];

/*   persons.forEach((String person) {
    print(person);
  });

  students.forEach((String person) {
    print(person);
  }); */

  persons.forEach(printName);
  students.forEach(printName);
}

void printName(String name) {
  print(name);
}

//Optional named parameters
//These are parameters added after the required ones
//These parameters are added inside a curly bracket
String combineName(String firstName, String lastName,
    {bool isLastNameFirstName = false}) {
  if (isLastNameFirstName) {
    return '$lastName, $firstName';
  } else {
    return '$firstName $lastName';
  }
}

String combineAddress(
    String specifics, String barangay, String city, String province) {
  return '$specifics, $barangay, $city, $province';
}

String getCompanyName() {
  return 'FFUF';
}

int getYearEstablishmed() {
  return 2017;
}

bool hasOnlineClasses() {
  return true;
}

//The initial isUnderAge function can be changed into a lambda (arrow) function
// A lambda function is a shortcut function for returning values from simple operations
//The syntax of a lambda function is:
// return-type function-name (parameters) => expression;
bool isUnderAge(age) => (age < 18) ? true : false;

Map<String, double> getCoordinates() {
  return {
    'latitude': 14.632702,
    'longitude': 121.043716,
  };
}
