void main() {
  print(getTotalPrice());
  print(getDiscount(20)(getTotalPrice()));
  print(getDiscount(40)(getTotalPrice()));
  print(getDiscount(60)(getTotalPrice()));
  print(getDiscount(80)(getTotalPrice()));
}

getTotalPrice() {
  List<num> priceList = [45, 34.2, 176.9, 32.2];

  num totalPrice = 0;

  for (var i = 0; i < priceList.length; i++) {
    totalPrice += priceList[i];
  }
  return totalPrice;
}

Function getDiscount(num percentage) {
  return (num amount) {
    return (100 - percentage) * amount / 100;
  };
}
