void main() {
  // assignment operator
  int x = 1397;
  int y = 7831;

  num sum = x + y;
  num difference = x - y;
  num product = x * y;
  num quotient = x / y;
  num remainder = x % y;
  num output = (x * y) - (x + y);

  bool isGreaterThan = x > y;
  bool isLessThan = x < y;

  bool isGTorEqual = x >= y;
  bool isLTorEqual = x <= y;

  bool isEqual = x == y;
  bool isNotEqual = x != y;

  bool isLegalAge = true;
  bool isRegistered = false;

  bool areAllRequirementsMet = isLegalAge && isRegistered;
  bool areSomeRequirementsMet = isLegalAge || isRegistered;

  print(sum);
  print(difference);
  print(product);
  print(quotient);
  print(remainder);
  print(output);
}
