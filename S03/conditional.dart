void main() {
  print(determineTyphoonIntensity(67));
  print(showServiceItems('robotics'));
  print(selectSector(3));
  print(isUnderAge);
}

List<String> showServiceItems(String category) {
  if (category == 'apps') {
    return ['native', 'android', 'ios', 'web'];
  } else if (category == 'cloud') {
    return ['azure', 'microservices'];
  } else if (category == 'robotics') {
    return ['sensors', 'fleet-tracking', 'realtime-commununication'];
  } else {
    return [];
  }
}

String determineTyphoonIntensity(int windSpeed) {
  if (windSpeed < 30) {
    return 'Not a typhoon yet';
  } else if (windSpeed <= 61) {
    return 'Tropical depression detected';
  } else if (windSpeed >= 62 && windSpeed <= 88) {
    return 'Tropical storm detected';
  } else if (windSpeed >= 89 && windSpeed <= 117) {
    return 'Severe tropical storm detected';
  } else {
    return 'Typhoon detected';
  }
}

String selectSector(int sectorID) {
  String name;

  switch (sectorID) {
    case 1:
      name = 'Craft';
      break;

    case 2:
      name = 'Assembly';
      break;

    case 3:
      name = 'Building operations';
      break;

    default:
      name = sectorID.toString() + ' is out of bounds.';
      break;
  }

  return name;
}

bool isUnderAge(age) {
  return age < 18;
}
